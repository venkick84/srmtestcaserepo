package srmTestCases;


import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import excelfunctions.readRulesExcel;
import excelfunctions.readVarExcel;
import utilities.tempVariables;


	public class testCases  {
		
		WebDriver driver;	
		String isVariablefound = null;
		String isRulefound = null;
		Boolean editRowsuccess = false;
		public tempVariables dataVars =new tempVariables();
		srmFunctions function = new srmFunctions();
				
		@BeforeMethod   	
		public void userlogin() throws Exception {
		
		/*Open Chrome Browser , disable notification */	
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-notifications");
			System.setProperty("webdriver.chrome.driver","C:\\Users\\vkrishnan\\git\\srmTestCaseRepo\\srmTestCaseRepo\\src\\main\\resources\\chromedriver.exe");
			driver =new ChromeDriver(options);
			
		/*login to the application */	
			driver = function.srmLogin(driver);
			dataVars.setdriver(driver);
			
		}
		
		
		public void deleteVars () throws Exception {
				
			File resourcesDirectory = new File("src/main/resources/variables.xlsx");
			String filepath = resourcesDirectory.getAbsolutePath();
			readVarExcel srmVarInput = new readVarExcel(filepath);
			
			String sheetName = "sheet1";
			int totRows = srmVarInput.getrowcount(sheetName);
			
			for (int exlrow = 0; exlrow < totRows + 1 ; exlrow++) {
				
			/*Get the Action and the Variable Type to determine which function to be invoked */
				String action = srmVarInput.getAction(exlrow);
				dataVars.setAction(action);
			/*Get the Variable name that needs to be deleted */	
				String varName = srmVarInput.getVarName(exlrow);
				dataVars.setvarName(varName);
			
			/*Perform Search function before and after delete, performing after Delete for confirmation*/
				
				if ((action.equalsIgnoreCase("Delete Variable"))) {
					
					isVariablefound = function.searchVar(dataVars,varName);
					dataVars.setisVariablefound(isVariablefound);
					Thread.sleep(3000);
					if (isVariablefound.equalsIgnoreCase("Yes")) {
						
						function.deleteVar(dataVars,varName);
						Thread.sleep(3000);
						isVariablefound = function.searchVar(dataVars,varName);
						
						if (isVariablefound.equalsIgnoreCase("No")) {
							System.out.println("Variable "+varName+" deleted successfully");
						}
						else {
							System.out.println("Variable "+varName+" not deleted");			
						}
					
					}
					
					else {
						System.out.println("Variable "+varName+" not found in SRM during search");
					}	
				}
			}
		}
		

		public void deleteRules () throws Exception {
			
			File resourcesDirectory = new File("src/main/resources/rules.xlsx");
			String filepath = resourcesDirectory.getAbsolutePath();
			readRulesExcel srmrulesInput = new readRulesExcel(filepath);
			String sheetName = "sheet1";
			int totRows = srmrulesInput.getrowcount(sheetName);
			
			for (int exlrow = 0; exlrow < totRows + 1 ; exlrow++) {
				
			/*Get the Action and the Variable Type to determine which function to be invoked */
				String ruleaction = srmrulesInput.getruleAction(exlrow);
				dataVars.setruleaction(ruleaction);
			/*Get the Variable name that needs to be deleted */	
				String ruleName = srmrulesInput.getruleName(exlrow);
				dataVars.setruleName(ruleName);
			
			/*Perform Search function before and after delete, performing after Delete for confirmation*/
				
				if ((ruleaction.equalsIgnoreCase("Delete Rule"))) {
					
					isRulefound = function.searchRule(dataVars,ruleName);
					dataVars.setisVariablefound(isRulefound);
					Thread.sleep(3000);
					if (isRulefound.equalsIgnoreCase("Yes")) {
						
						function.deleteRule(dataVars,ruleName);
						Thread.sleep(3000);
						isRulefound = function.searchRule(dataVars,ruleName);
						
						if (isRulefound.equalsIgnoreCase("No")) {
							System.out.println("Rule "+ruleName+" deleted successfully");
						}
						else {
							System.out.println("Rule "+ruleName+" not deleted");			
						}
					}
					
					else {
						System.out.println("Rule "+ruleName+" not found in SRM during search");
					}				
				}		
			}
		}
		
		
		@Test
		public void editRule() throws Exception {
			
			File resourcesDirectory = new File("src/main/resources/variables.xlsx");
			String filepath = resourcesDirectory.getAbsolutePath();
			readRulesExcel srmrulesInput = new readRulesExcel(filepath);
			String sheetName = "sheet1";
			String actualRuleedited = "";
			int nbrofDlts = 0;
			int totRows = srmrulesInput.getrowcount(sheetName);
			
			for (int exlrow = 0; exlrow < totRows + 1 ; exlrow++) {
				
			/*Get the Action and the Variable Type to determine which function to be invoked */
				String ruleaction = srmrulesInput.getruleAction(exlrow);
				dataVars.setruleaction(ruleaction);
			/*Get the Rule name that needs to be updated/deleted */	
				String ruleName = srmrulesInput.getruleName(exlrow);
				dataVars.setruleName(ruleName);
			/*Get the Variable Name associated to the Rule that is modified / deleted*/	
				String ruleSort = srmrulesInput.getsorting(exlrow);
				dataVars.setruleSort(ruleSort);
				String Sort1 = dataVars.getruleSort();
				
			/*Save the previously edited rule*/
				if ((ruleaction.equalsIgnoreCase("Edit Rule") || ruleaction.equalsIgnoreCase("Delete Rule") ||
												ruleaction.equalsIgnoreCase("New Rule")) && editRowsuccess == true) {	
					function.saveLastedit(dataVars);		
					editRowsuccess = false;
					nbrofDlts = 0;
				}
				
				
			/*Perform Search function before and after delete, performing after Delete for confirmation*/
				if ((ruleaction.equalsIgnoreCase("Edit Rule"))) {	
					
					actualRuleedited = srmrulesInput.getruleName(exlrow);
					dataVars.setactualRuleedited(actualRuleedited);
					System.out.println("\n");
					System.out.println("Rule Name for editing is "+ ruleName);
					System.out.println("*****************************************************");
					
					isRulefound = function.searchRule(dataVars,ruleName);
					dataVars.setisVariablefound(isRulefound);
					editRowsuccess = true;
				}		
			
				
			/*If the searched Rule is found, Click Edit to make changes */					
				if ((ruleaction.equalsIgnoreCase("Edit Row")) && isRulefound.equalsIgnoreCase("Yes") && editRowsuccess.equals(true)) {
					editRowsuccess = function.updateRowonRule(dataVars, srmrulesInput, exlrow); 	
				}
				
				
			/* Add Condition if a Regular expression is switched to Conditional expression */	
				if (ruleaction.equalsIgnoreCase("Add Condition") && editRowsuccess.equals(true) && isRulefound.equalsIgnoreCase("Yes")) {
					editRowsuccess = function.addConditionRule(dataVars, srmrulesInput, exlrow);
				}
				
			
			/* Delete an Expression from a Rule , input the row number to Delete*/	
				if (ruleaction.equalsIgnoreCase("Delete Row") && editRowsuccess.equals(true) && isRulefound.equalsIgnoreCase("Yes")) {

					int ruleName1 = Integer.valueOf(ruleName) - nbrofDlts;
					editRowsuccess = function.deleteExpression(dataVars, ruleName1);
					nbrofDlts++;
				}
				
			}				
		}
		
		
			
		@AfterMethod
		public void closeBrowser() {
			
			dataVars.getdriver().close();
			dataVars.getdriver().quit();
			
		}



}
