package srmTestCases;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import excelfunctions.readRulesExcel;
import utilities.tempVariables;

public class srmFunctions {
	
/*Check the GIT commit */
	
	public WebDriver srmLogin(WebDriver driver) throws Exception {
			
		Thread.sleep(3000);
		driver.get("https://srmqa.homeoffice.anfcorp.com/srm/login.action");
		Thread.sleep(5000);
		driver.manage().window().maximize();
		
		driver.findElement(By.name("j_username")).sendKeys("SRM_Test_OMUser");
		Thread.sleep(2000);
		driver.findElement(By.name("j_password")).sendKeys("$RMl3tm3!n3");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//input[@value='Submit']")).click();
		Thread.sleep(5000);
		
		Boolean login = false;
		try {
			 login = driver.findElement(By.xpath("//div/i[@class='fa fa-exclamation-triangle']")).isDisplayed();
			if(login == true) {
				System.out.println("SRM is currently down");
				driver.quit();
			}
		} catch (org.openqa.selenium.NoSuchElementException e) {
			System.out.println("Login Successful");

		}
		return driver;
	}
	
	
	
	public String searchVar(tempVariables dataVars,String varName) throws Exception {
		
		String isVariablefound;
		
		dataVars.getdriver().navigate().refresh();
		
		Thread.sleep(5000);
		dataVars.getdriver().findElement(By.xpath("//li/a[contains(text(),'VARIABLE')]")).click();
		Thread.sleep(2000);
		
		dataVars.getdriver().findElement(By.xpath("//div/input[@ng-model = 'nav.variableSearch']")).click();
		Thread.sleep(2000);
		
		dataVars.getdriver().findElement(By.xpath("//div/input[@ng-model = 'nav.variableSearch']")).sendKeys(varName);
		Thread.sleep(2000);
		
		try {
			dataVars.getdriver().findElement(By.xpath("//div/span/b[text() = '"+varName+"']")).click();
			System.out.println("Search complete");
			isVariablefound = "Yes";

		} catch (NoSuchElementException e) {
			System.out.println("Variable "+varName+" not found");
			isVariablefound = "No";
	
		}
	return isVariablefound;
	}



	public void deleteVar(tempVariables dataVars, String varName) throws Exception {
		
		Boolean displayed = dataVars.getdriver().findElement(By.xpath("//div/label[text() = '"+varName+"']")).isDisplayed();
		
		if (displayed == true) {
			dataVars.getdriver().findElement(By.xpath("//div/button[@ng-click='aoctrl.deleteAOVariable()']")).click();
			Thread.sleep(3000);
			System.out.println("Variable "+varName+" deleted");
		}
		else {
			System.out.println("Variable "+varName+" not deleted");
		}
		
	}
	

	public String searchRule(tempVariables dataVars,String ruleName) throws Exception {
		
		String isRulefound;
		
		dataVars.getdriver().navigate().refresh();
		
		Thread.sleep(10000);
	/*Click on Rules tab*/	
		dataVars.getdriver().findElement(By.xpath("//li/a[@class = 'nav-link ng-binding' and contains(text(),'RULES')]")).click();
		Thread.sleep(2000);
		
	/*Click on Rules search input field */	
		dataVars.getdriver().findElement(By.xpath("//div/input[@ng-model = 'nav.ruleSearch']")).click();
		Thread.sleep(2000);
		
		dataVars.getdriver().findElement(By.xpath("//div/input[@ng-model = 'nav.ruleSearch']")).sendKeys(ruleName);
		Thread.sleep(2000);
		
		try {
			dataVars.getdriver().findElement(By.xpath("//div/span/b[text() = '"+ruleName+"']")).click();
			System.out.println("Search complete");
			isRulefound = "Yes";

		} catch (NoSuchElementException e) {
			System.out.println("Rule "+ruleName+" not found");
			isRulefound = "No";
	
		}
	return isRulefound;
	}



	public void deleteRule(tempVariables dataVars, String ruleName) throws Exception {
	
		Boolean displayed = dataVars.getdriver().findElement(By.xpath("//div/label[text() = '"+ruleName+"']")).isDisplayed();
		
		if (displayed == true) {
			dataVars.getdriver().findElement(By.xpath("//div/button[@ng-click='rulesCtrl.deleteRule()']")).click();
			Thread.sleep(3000);
			System.out.println("Rule "+ruleName+" deleted");
		}
		else {
			System.out.println("Rule "+ruleName+" not deleted");
		}
	
	}


/*********************************** UPDATE EXPRESSSIONS ************************************************/
	public Boolean updateRowonRule(tempVariables dataVars,readRulesExcel srmrulesInput , int exlrow) throws Exception {
		Boolean editRowsuccess = true;
		Boolean orderUpdated = false;
		
		String currruleSelection;
		String currOrdSelection;
		String currLvlSelection;
		String currValSelection;
		
		String ruleName = srmrulesInput.getruleName(exlrow).trim();
		String sorting = srmrulesInput.getsorting(exlrow).trim();
		String order = srmrulesInput.getOrder(exlrow).trim();
		String level = srmrulesInput.getLevel(exlrow).trim();
		String newProdOvr = srmrulesInput.getnewProdOvr(exlrow).trim();
		String cndinpval = srmrulesInput.getcndinpval(exlrow).trim();
		String Conditionalflag =  srmrulesInput.getConditionalflag(exlrow).trim();
		
		Thread.sleep(3000);
		try {
			dataVars.getdriver().findElement(By.xpath("//div/button[@ng-click='rulesCtrl.switchToEditMode()']")).click();
		} catch (NoSuchElementException e) {

		}
		
		Boolean rowavailable =  dataVars.getdriver().findElement(By.xpath(" //*[@id='viewcontainer']//table/tbody[1]/tr/td[1]")).isDisplayed();
		
		if (rowavailable == true)
		{
		/*Editing selection Variable*/
			
			if (dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[2]//dt-dropdown-toggle/button/span ")).isEnabled()) {
			
			/*Compare currently displayed value against the excel input*/
				currruleSelection = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[2]//dt-dropdown-toggle/button/span ")).getText();
				Thread.sleep(2000);	
				
				if (!currruleSelection.equalsIgnoreCase(sorting)){ 
						
				/*Click on Select Variable tab to view the drop down */
					dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[2]//dt-dropdown-toggle/button/span ")).click();
						
				/*Enter the attribute for search in the Search box and filter*/	
					dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody['"+ruleName+"']/tr/td[2]//div/input")).sendKeys(sorting);
					Thread.sleep(2000);
					
				/*Select from resulting drop down*/		
					try {
						dataVars.getdriver().findElement(By.xpath("//div/span[@ng-keydown = '$ctrl.keyDown($event)' and text() = '"+sorting+"']")).click();
						Thread.sleep(3000);
					} catch (Exception e) {
						editRowsuccess = false;
						System.out.println("No values displayed for filter "+sorting+" for selection");
					}
					
					if (editRowsuccess.equals(true))
					{
						
					/*Editing selection Order*/
						if (dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[2]/div/select")).isEnabled()) {
							WebElement mySelect3 = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName.trim()+"]/tr/td[4]/span[2]/div/select"));
							Select dropdown3 = new Select(mySelect3);
							try {
								dropdown3.selectByVisibleText(order.trim());
								Thread.sleep(3000);
								System.out.println(order+ " dropdown is selected");				
								orderUpdated =  true;
							} catch (NoSuchElementException e) {
								editRowsuccess = false;
								System.out.println("Value input "+order+ " not available in the dropdown for selection");
							}						
					}
					else {
						System.out.println("Order selection dropdown is disabled");
					}
						
					}		
				}
			}	
			else {
				System.out.println("Variable selection dropdown is disabled");
			}
			
			
		/*If Order field not updated as a part of Variable field update, then check for Order field updates */
			if (dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[2]/div/select")).isEnabled() && orderUpdated == false) {
				
				currOrdSelection = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[2]/div/select")).getText();	
				if (!currOrdSelection.equalsIgnoreCase(order)) {
					WebElement mySelect3 = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[2]/div/select"));
					Select dropdown3 = new Select(mySelect3);
					try {
						dropdown3.selectByVisibleText(order);
						Thread.sleep(3000);
						System.out.println(order+ " dropdown is selected");				
						orderUpdated =  true;
					} catch (NoSuchElementException e) {
						editRowsuccess = false;
						System.out.println("Value input "+order+ " not available in the dropdown for selection");
					}							
				}
				else {
					System.out.println("No changes on Order field");
				}
				
			}
			

			
		/*Editing selection Level*/
			if (dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[3]/span[2]/div/select")).isEnabled()) {
				
		/*Compare currently displayed value against the excel input*/
					currLvlSelection = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[3]/span[2]/div/select")).getText();
					Thread.sleep(2000);	
					
					if (!currLvlSelection.equalsIgnoreCase(level.trim())){ 
						WebElement mySelect = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[3]/span[2]/div/select"));
						Select dropdown = new Select(mySelect); 
						try {
							dropdown.selectByVisibleText(level);
							Thread.sleep(3000);
							System.out.println(level+ " dropdown is selected");
						} catch (NoSuchElementException e) {
	
							editRowsuccess = false;
							System.out.println("Value input "+level+ " not Valid");
						}
					}
					else {
						System.out.println("Level selection dropdown is disabled");
					}
			}
			
			
		
		/*Editing input for Numeric operator Orders*/
			try {
				
				if (dataVars.getdriver().findElement(By.xpath(("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[4]/div/input"))).isDisplayed() == true) {
					
		/*Compare currently displayed value against the excel input*/
						currValSelection = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[4]/div/input")).getText();
						Thread.sleep(2000);	
						
						if (!currValSelection.equals(cndinpval.trim())){ 
							try {
								dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[4]/span[2]/div/select")).sendKeys(cndinpval);
							} catch (Exception e) {
								editRowsuccess = false;
								System.out.println(cndinpval+" is not a value in the dropdown.Proceed with next rule.");
							}						
						}
					}
	
			} catch (NoSuchElementException e) {
	
			}	
			
			
		/*Editing selection New Product Override*/
			if (dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[5]/span[2]/div/select")).isEnabled()) {
				
		/*Compare currently displayed value against the excel input*/
					String  currSelection = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[5]/span[2]/div/select")).getText();
					Thread.sleep(2000);	
					
					if (!currSelection.equalsIgnoreCase(newProdOvr.trim())){ 
							WebElement mySelect3 = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr/td[5]/span[2]/div/select"));
							Select dropdown3 = new Select(mySelect3);
							try {
								dropdown3.selectByVisibleText(newProdOvr);
								Thread.sleep(3000);
								System.out.println(newProdOvr+ " dropdown is selected");				
								
							} catch (NoSuchElementException e) {
								editRowsuccess = false;
								System.out.println("Value input "+newProdOvr+ " not available in the dropdown for selection");
							}
						}						
					}
			else {
				System.out.println("New Product Override selection dropdown is disabled");
			}
		
			
		/*Updating the Condition flag*/	
			
			if (dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr[1]/td[6]/div/input")).isEnabled()) {
				
				Boolean isChecked = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr[1]/td[6]/div/input")).isSelected();
				if((isChecked == true && Conditionalflag.equals("N")) || (isChecked == false && Conditionalflag.equals("Y")) ) {
					dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr[1]/td[6]/div/input")).click();
				}
			}
			else {
				System.out.println("Condition selection checkbox is disabled");
			}
		}
		else {
			editRowsuccess = false;
			System.out.println("Row to edit is not displayed under the Rule");
		}
		
		return editRowsuccess;
	}



	public void saveLastedit(tempVariables dataVars) throws Exception {
		
		dataVars.getdriver().findElement(By.xpath("//div/button[@type = 'submit']")).click();
		Thread.sleep(5000);
		Boolean saveError = false;	
	/*Check to verify Save Rule was successful */
		try {
			saveError = dataVars.getdriver().findElement(By.xpath("//div[2]//ul/li/b[@class = 'unsaved ng-scope' and text() = ' ! ']")).isDisplayed();
		} catch (NoSuchElementException e) {
		}
			
		if (saveError == true) {
			System.out.println("Save was not successful for the Rule "+ dataVars.getactualRuleedited());
			System.out.println("*********************************************************");
			Thread.sleep(3000);
		/*Cancel the Editing that failed*/
			dataVars.getdriver().findElement(By.xpath("//div/button[@ng-click='rulesCtrl.showCancelWarning()']")).click();
			Thread.sleep(3000);
			dataVars.getdriver().findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/button[1]")).click();
			Thread.sleep(3000);		
		}
		else {
			System.out.println("Rule Created successfully");
			System.out.println("*************************");
		}
	}


	/******************************************** DELETE EXPRESSSIONS **********************************************/
	public Boolean deleteExpression(tempVariables dataVars, int ruleNameint) {
		
		String ruleName = Integer.toString(ruleNameint);  
		Boolean editRowsuccess = true;
		String exlvarToDelete = dataVars.getruleSort();
		
	/* Switch to Edit mode */	
		try {
			dataVars.getdriver().findElement(By.xpath("//div/button[@ng-click='rulesCtrl.switchToEditMode()']")).click();
		} catch (NoSuchElementException e) {

		}
		
	/* Compare if the Variable input in the excel matches with the Variable Name displayed on UI for the for the row number */ 	
		String varDisplayed = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]//dt-dropdown-toggle/button/span/span[1]")).getText();
		if (exlvarToDelete.equalsIgnoreCase(varDisplayed)) {
	
		/*Verify if the row to delete is available under the rule , and click on delete button*/	
			Boolean rowavailable =  dataVars.getdriver().findElement(By.xpath(" //*[@id='viewcontainer']//table/tbody[1]/tr/td[1]")).isDisplayed();		
			if (rowavailable == true)
			{
				dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+ruleName+"]/tr[1]/td[7]/b")).click();
			}	
			else {
				editRowsuccess = false;
				System.out.println("Expression to delete is not displayed under the Rule");
			}	
		}
		return editRowsuccess;
	}


/******************************************** ADD CONDITION FOR EDIT RULE **********************************************/
	public Boolean addConditionRule(tempVariables dataVars, readRulesExcel srmrulesInput, int exlrow) throws Exception {
		Boolean editRowsuccess = true;
		Boolean isenabled;
		int condNbr = 0;
		String condRownbr = " ";
		String ruleName = srmrulesInput.getruleName(exlrow).trim();
		String sorting = srmrulesInput.getsorting(exlrow).trim();
		String order = srmrulesInput.getOrder(exlrow).trim();
		String level = srmrulesInput.getLevel(exlrow).trim();
		
		if (ruleName.equalsIgnoreCase("true")){
			condNbr = 2;
			condRownbr =  srmrulesInput.getruleName(exlrow - 1).trim();
		}
		else if (ruleName.equalsIgnoreCase("false")) {
			condNbr = 3;
			condRownbr =  srmrulesInput.getruleName(exlrow - 2).trim();
		}
		else {
			System.out.println("True & False conditions should be listed in the input while flipping Conditional? field to Y " + dataVars.getactualRuleedited());
			editRowsuccess = false;
			
		}
		
		if ( editRowsuccess == true) {
		/*Make sure all 3 fields for Add conditions are displayed on the UI */ 	
			try {
				dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[2]//dropdown-tree/div/dt-dropdown-toggle/button/span/span[1]")).isDisplayed();
				dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[3]/div/div/span[2]/div/select")).isDisplayed();
				dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[4]/div/div/span[2]/div/select")).isDisplayed();
			} catch (NoSuchElementException e) {
				editRowsuccess = false;
				System.out.println("One or more mandatory fields missing for Add Condition function");
			}
		
		/*If Variables field is enabled , select User input value. Stop the process if this field is disabled */	
			if (editRowsuccess == true) {
				
				isenabled = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[2]//dropdown-tree/div/dt-dropdown-toggle/button/span/span[1]")).isEnabled();
				if (isenabled == true) {
					dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[2]//dropdown-tree/div/dt-dropdown-toggle/button/span/span[1]")).click();
					Thread.sleep(2000);
					
					dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[2]//dropdown-tree/div/div/div/input")).sendKeys(sorting);
					Thread.sleep(3000);
					
				/*Select from resulting drop down*/		
					try {
						dataVars.getdriver().findElement(By.xpath("//div/span[@ng-keydown = '$ctrl.keyDown($event)' and text() = '"+sorting+"']")).click();
						Thread.sleep(3000);
					} catch (Exception e) {
						editRowsuccess = false;
						System.out.println("No values displayed for filter "+sorting+" for selection");
					}	
				}
				else {
					System.out.println("Variable field is disabled");
					editRowsuccess = false;
				}
				
			/* If Variables field input was success, proceed with Level field selection */	
					if (editRowsuccess == true) {
					
						isenabled = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[3]/div/div/span[2]/div/select")).isEnabled();
						if (isenabled == true) {
							dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[3]/div/div/span[2]/div/select")).click();
							
							WebElement mySelect3 = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[3]/div/div/span[2]/div/select"));
							Select dropdown3 = new Select(mySelect3);
							try {
								dropdown3.selectByVisibleText(level);
								Thread.sleep(3000);
								System.out.println(level+ " dropdown is selected");				
							} catch (NoSuchElementException e) {
								editRowsuccess = false;
								System.out.println("Value input "+level+ " not available in the dropdown for selection");
							}							
						}
						else {
							System.out.println("Level field is disabled");
						}
						
						/* If Level field input did not have any failures, proceed with Order field selection */	
							if (editRowsuccess == true) {
							
								isenabled = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[4]/div/div/span[2]/div/select")).isEnabled();
								if (isenabled == true) {
									dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[4]/div/div/span[2]/div/select")).click();
									
									WebElement mySelect4 = dataVars.getdriver().findElement(By.xpath("//*[@id='viewcontainer']//tbody["+condRownbr+"]/tr["+condNbr+"]/td[3]/div/div/span[2]/div/select"));
									Select dropdown4 = new Select(mySelect4);
									try {
										dropdown4.selectByVisibleText(order);
										Thread.sleep(3000);
										System.out.println(order+ " dropdown is selected");				
									} catch (NoSuchElementException e) {
										editRowsuccess = false;
										System.out.println("Value input "+order+ " not available in the dropdown for selection");
									}							
									
								}
								else {
									System.out.println("Order field is disabled");
								}
							
							}
					
					}
				
				}
		}
		return editRowsuccess;
		
	}	
	
}
