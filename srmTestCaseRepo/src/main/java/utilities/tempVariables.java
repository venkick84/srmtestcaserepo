package utilities;

import org.openqa.selenium.WebDriver;

public class tempVariables {

	/* Driver */
	private WebDriver driver;
	public WebDriver getdriver() {
		return driver;
	}
	
	public void setdriver(WebDriver driver) {
		this.driver = driver;	
	}

	/*************************** Variable excel Start***************************************/
	/* Action from excel input */
	private String action;
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		action = this.action;
	}
	
	/* Variable Type from Input */
	private String varType;
	public String getvarType() {
		return varType;
	}
	
	public void setvarType(String varType) {
		varType = this.varType;
	}
	
	
	/*Variable Name from excel input*/
	private String varName;
	public String getvarName() {
		return varName;
	}
	
	public void setvarName(String varName) {
		varName = this.varName;
	}
	
	/*************************** Variable excel End***************************************/
	
	/*************************** Rules excel Start***************************************/
	
	/* Rule Action from excel input */
	private String ruleaction;
	public String getruleaction() {
		return ruleaction;
	}
	
	public void setruleaction(String ruleaction) {
		ruleaction = this.ruleaction;
	}
	
	
	/* Rule Name from excel input */
	private String ruleName;
	public String getruleName() {
		return ruleName;
	}
	
	public void setruleName(String ruleName) {
		ruleName = this.ruleName;
	}
	
	
	/* Rule Name from excel input */
	private String getruleSort;
	public String getruleSort() {
		return getruleSort;
	}

	public void setruleSort(String getruleSort) {
		this.getruleSort = getruleSort;
	}
	
	
	/***************************************************************************************/	
	
	/*Variable Name from excel input*/
	private String isVariablefound;
	public String getisVariablefound() {
		return isVariablefound;
	}
	
	public void setisVariablefound(String isVariablefound) {
		isVariablefound = this.isVariablefound;
	}
	
	
	/*Variable Name from excel input*/
	private String isRulefound;
	public String getisRulefound() {
		return isRulefound;
	}
	
	public void setisRulefound(String isRulefound) {
		isRulefound = this.isRulefound;
	}
	
	
	/*Variable Name from excel input*/
	private String actualRuleedited;
	public String getactualRuleedited() {
		return isRulefound;
	}
	
	public void setactualRuleedited(String actualRuleedited) {
		actualRuleedited = this.actualRuleedited;
	}
	
	
	
	
}
