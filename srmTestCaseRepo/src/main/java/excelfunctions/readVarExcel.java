package excelfunctions;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class readVarExcel {

	public XSSFWorkbook workbook;
	public XSSFSheet worksheet;
	public FileInputStream FIS;
	public File excelInput;
	public XSSFRow row;
	public XSSFCell cell;


	public readVarExcel(String fileloc) throws Exception {
		excelInput = new File(fileloc);
		FIS = new FileInputStream(excelInput);
		workbook = new XSSFWorkbook(FIS);
		worksheet = workbook.getSheetAt(0);
		
		System.out.println("File location - "+fileloc);
	}
	
	public int getrowcount(String sheetName) {
		worksheet = workbook.getSheet(sheetName);
		int totRows = worksheet.getLastRowNum();
		System.out.println("Total Rows in the Input Excel are " + totRows);
		System.out.println("***************************************");
		
		return totRows;
	}

	public String getAction(int row) {
		String action;
		action = worksheet.getRow(row).getCell(0).getStringCellValue();
		return action;	
	}

	public String getvarType(int row) {
		String varType;
		try {
			varType = worksheet.getRow(row).getCell(1).getStringCellValue();

		} catch (NullPointerException e) {
			varType = "  ";
		}			
		//System.out.println("Variable Type to Create is "+ varType);
		return varType;	
	}
	
	public String getVarName(int row) {
		String varName;
		try {
			varName = worksheet.getRow(row).getCell(2).getStringCellValue();

		} catch (NullPointerException e) {
			varName = "  ";
		}			
		System.out.println("Variable Name is "+ varName);
		return varName;	
	}
	
	public String getBlendorBoost(int row) {
		String blendorBoost;
		try {
			blendorBoost = worksheet.getRow(row).getCell(3).getStringCellValue();

		} catch (NullPointerException e) {
			blendorBoost = "  ";
		}			
		System.out.println("Type selection is "+ blendorBoost);
		return blendorBoost;	
	}
	
	public String getAttr(int row) {
		String Attribute;
		try {
			Attribute = worksheet.getRow(row).getCell(4).getStringCellValue();

		} catch (NullPointerException e) {
			Attribute = "  ";
		}			
		System.out.println("Attribute Name is "+ Attribute);
		return Attribute;	
	}
	
	public String getlevel(int row) {
		String level;
		try {
			level = worksheet.getRow(row).getCell(5).getStringCellValue();

		} catch (NullPointerException e) {
			level = "  ";
		}			
		System.out.println("Level selection is "+ level);
		return level;	
	}
	
	
	public String getorderofSort(int row) {
		String orderofSort;
		try {
			orderofSort = worksheet.getRow(row).getCell(6).getStringCellValue();

		} catch (NullPointerException e) {
			orderofSort = "  ";
		}			
		System.out.println("Order for sorting is "+ orderofSort);
		return orderofSort;	
	}
	
	public String getWeight(int row) {
		String weight;
		weight = worksheet.getRow(row).getCell(7).getStringCellValue();
		System.out.println("Weight/Attribute Value input is "+ weight);
		return weight;	
	}

	public String getdefaultval(int row) {
		String defaultval;
		try {
			defaultval = worksheet.getRow(row).getCell(8).getStringCellValue();
		} catch (NullPointerException e) {
			defaultval = " ";
		}
		System.out.println("Default value = "+ defaultval);
		return defaultval;
	}
	
	
	
}
