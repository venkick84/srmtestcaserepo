package excelfunctions;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class readRulesExcel {
	public XSSFWorkbook workbook;
	public XSSFSheet worksheet;
	public FileInputStream FIS;
	public File excelInput;
	public XSSFRow row;
	public XSSFCell cell;

	public readRulesExcel(String fileloc) throws Exception {

		excelInput = new File(fileloc);
		FIS = new FileInputStream(excelInput);
		workbook = new XSSFWorkbook(FIS);
		worksheet = workbook.getSheetAt(0);
	}

	public int getrowcount(String sheetName) {

		worksheet = workbook.getSheet(sheetName);
		int totRows = worksheet.getLastRowNum();
		System.out.println("Total Rows in the Input Excel are " + totRows);
		return totRows;
	}

	public String getruleAction(int row) {
		String ruleaction;
		try {
			ruleaction = worksheet.getRow(row).getCell(0).getStringCellValue();
		} catch (NullPointerException e) {
			ruleaction = " ";
		}
		return ruleaction;
	}

	public String getruleName(int row) {
		
		String ruleName;
		try {
			ruleName = worksheet.getRow(row).getCell(1).getStringCellValue();

		} catch (NullPointerException e) {
			ruleName = "  ";
		}			
		return ruleName;	
	}

	public String getsorting(int exlrow) {

		String sorting ; 
		
		try {
			sorting = worksheet.getRow(exlrow).getCell(2).getStringCellValue();

		} catch (NullPointerException e) {
			sorting = " ";
		}	
		
		System.out.println("sorting = "+ sorting);
		
		
		return sorting;
	}

	public String getOrder(int exlrow) {
		
		String order = worksheet.getRow(exlrow).getCell(4).getStringCellValue();
		System.out.println("order = "+ order);
		return order;
	}

	public String getLevel(int exlrow) {
		
		String level = worksheet.getRow(exlrow).getCell(3).getStringCellValue();
		System.out.println("Level = "+ level);
		return level;
	}

	public String getnewProdOvr(int exlrow) {
		
		String newProdOvr;
		try {
			newProdOvr = worksheet.getRow(exlrow).getCell(6).getStringCellValue();
		} catch (NullPointerException e) {
			newProdOvr = " ";
		}
		System.out.println("New Product override = "+ newProdOvr);
		return newProdOvr;
	}

	public String getcndinpval(int exlrow) {
		
		String cndinpval;
		try {
			cndinpval = worksheet.getRow(exlrow).getCell(5).getStringCellValue();
		} catch (NullPointerException e) {
			cndinpval = " ";
		}
		System.out.println("Condition input value for Order  = "+ cndinpval);
				return cndinpval;
	}

	public String getConditionalflag(int exlrow) {
		
		String conditionalflag;
		try {
			conditionalflag = worksheet.getRow(exlrow).getCell(7).getStringCellValue();
		} catch (NullPointerException e) {
			conditionalflag = " ";
		}
		System.out.println("Conditional flag = "+ conditionalflag);
		return conditionalflag;
	}
		

}
